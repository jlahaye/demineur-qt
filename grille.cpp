#include "grille.h"
#include<cstdlib>
#include<ctime>

using namespace std;

Grille::Grille(int t, int n){
    taille=t;
    grille=vector(taille,vector(taille,-1));
    mines=vector(taille,vector(taille,false));
    nb=n;
    for(int i=0;i<nb;i++){
        mines[rand()%taille,rand()%taille]=true;    //placement des mines
    };
}

void Grille::joue(int x, int y){
    if(mines[x,y]) perdu();
            else{
        decouvre(x,y);
    }
}
