#ifndef F_MAINWIDGET_H
#define F_MAINWIDGET_H

#include <QWidget>
#include <QGridLayout>

class MainWidget
{
	Q_OBJECT
private:
		QGridLayout *layout;
public:
		MainWidget (QWidget *parent = nullptr);
		~MainWidget();
};


#endif // defined F_MAINWIDGET_H
