#ifndef GRILLE
#define GRILLE
#include <vector>

using namespace std;

class Grille{
private :
            int taille;
            int nb;
            vector<vector<int>> grille;
            vector<vector<bool>> mines;
            void decouvre (int x, int y);
            void perdu();
public :
            Grille(int t, int n);
            void nouvelle_partie(int t);
            void joue(int x, int y);
};

#endif // GRILLE

